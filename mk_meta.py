'''
Created on Jul 30, 2015

@author: roman
'''
from sql import SQLdata
import sys

if __name__ == '__main__':
    data_db=SQLdata(sys.argv[1])
    meta_db=SQLdata(sys.argv[2])
    where=sys.argv[3]
    
    meta_db.initdata({'refcode':'TEXT',
                      'RUN':'INTEGER',
                      'OK':'INTEGER'})
    
    for d in data_db.select_data(('refcode',), where):
        meta_db.add_data({'refcode':d['refcode'],
                          'RUN': '0',
                          'OK': '0'} )
        
    meta_db.create_idx('refcode')
    
    data_db.close()
    meta_db.close()