'''
Created on Jul 31, 2015

@author: roman
'''

from run_tmpl import *
import sys, re
from iotbx import cif
import cStringIO

db_data=sys.argv[1]
db_meta=sys.argv[2]
db_cifmeta=sys.argv[3]

CIFKEYS={'_journal_coden_Cambridge':('integer',int),
     '_journal_year':('integer',int),
     '_journal_name_full':('text',str),
     '_diffrn_ambient_temperature':('real',float),
     '_diffrn_ambient_pressure':('real',float),
     '_exptl_crystal_description':('text',str),
     '_cell_volume':('real',float),
     '_cell_formula_units_Z':('integer',int),
     '_refine_ls_R_factor_gt':('real',float),
     '_symmetry_Int_Tables_number':('integer',int),
     '_diffrn_radiation_probe':('text',str),
     '_len_symmetry_equiv_pos_site_id':('integer',int),
     '_list_atom_type_symbol': ('text',str)
    }

workdata=('refcode','cif_z','acc_z','spf_z', 'lis_z')

def getkeys(cifdict):
    res={}
    for key in CIFKEYS.keys():
        if (key in cifdict.keys()) or \
                    (key.startswith('_len_') and key[4:] in cifdict.keys()) or \
                    (key.startswith('_list_') and key[5:] in cifdict.keys()):
            if key.startswith('_len_'):
                v=len(cifdict[key[4:]])
            elif key.startswith('_list_'):
                v=','.join(cifdict[key[5:]])+','
            else:
                v=cifdict[key]
            try:
                res[key]=CIFKEYS[key][1](v)
            except:
                pass
    return res

def nres(spf):
    nres=0
    for line in spf:
        if line.startswith(''):
            nres+1
    return nres
    

schema=dict()
for key,value in CIFKEYS.items():
    schema[key]=value[0]

schema['refcode']="TEXT"
schema['hasH']="INTEGER"
schema['nMols']="INTEGER"
schema['zprime']="REAL"
schema['formulaOK']="INTEGER"

db=SQLdata(db_cifmeta)
db.initdata(schema)
db.close()

def dowork(data):
    print data['refcode']
    
    reader_cif=cif.reader(input_string=data['cif_z'])
#    reader_acc=cif.reader(input_string=data['acc_z'])
    
    cifmodel_cif=reader_cif.model().values()[0]
#    cifmodel_acc=reader_acc.model().values()[0]
    
    res=getkeys(cifmodel_cif)
#    cifdict_acc=getkeys(cifmodel_acc)
    
#    struct_cif=reader_cif.build_crystal_structures().values()[0]
#    struct_acc=reader_cif.build_crystal_structures().values()[0]
    
    for key in CIFKEYS.keys():
        if not key in res.keys():
            res[key]=None
    res['refcode']=data['refcode']



    res['nMols']=data['spf_z'].count('\nRESD')
    res['hasH']=int(('H,' in res['_list_atom_type_symbol']) or \
                    ('D,' in res['_list_atom_type_symbol']))

    #z
    f=cStringIO.StringIO(data['lis_z'])
    for line in f:
        if line.startswith('# Z '):
            res['_cell_formula_units_Z']=int(line[14:23])
            zrep=float(line[31:40])
            break
    f.close()
    #zprime
    f=cStringIO.StringIO(data['lis_z'])
    for line in f:
        if line.startswith('            SpaceGroup_Z ='):
            res['zprime']=float(line.split()[-1])
            break
    f.close()
    #formula check
    r=re.compile('([a-zA-Z]+)(\d+\.?\d*)?')
    f=cStringIO.StringIO(data['lis_z'])
    if zrep==0:
        zrat=1
    else:
        zrat=zrep/float(res['_cell_formula_units_Z'])
    for line in f:
        if line.startswith('#    SumFormula'):
            l=(line, f.next())
            fml=(dict(), dict())
            for q in (0,1):
                for el,count in [r.search(s).groups() for s in l[q].split()[2:]]:
                    if not count:
                        count=1.0
                    count=float(count)
                    if q==1:
                        count*=zrat
                    fml[q][el]=round(count)
            res['formulaOK']=int(fml[0]==fml[1])
            if not res['formulaOK']:
                print fml[0]
                print fml[1]
            break
    f.close()
    
    db=SQLdata(db_cifmeta)
    db.add_data(res)
    db.close()

run(db_data,db_meta,dowork,workdata)