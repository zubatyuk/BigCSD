'''
Created on Jul 31, 2015

@author: roman
'''

import sys,os
import glob,zlib
from sql import SQLdata

if __name__ == '__main__':
    dbname=sys.argv[1]
    files=glob.glob(sys.argv[2])
    
    db=SQLdata(dbname)
    #db.initdata({'mopinpz':'BLOB'})
    db.add_column({'accz':'BLOB'})
    
    for fil in files:
        print fil
        refcode=os.path.splitext(os.path.basename(fil))[0][-4]
        data=file(fil).read()
        db.add_data(data={'accz':data},compress=('accz',))
        
    db.close()