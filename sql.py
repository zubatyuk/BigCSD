'''
Created on Jul 29, 2015

@author: roman
'''

import sqlite3
import zlib

class SQLdata(object):
    '''
    helper class for sqlite3 tables
    '''

    def __init__(self, dbname):
        self.connect(dbname)
        
    def _templ1(self,nval):
        t=str()
        for _ in xrange(nval):
            t+='?,'
        return t[:-1]
        
    def _templ2(self,keys):
        t=str()
        for i in xrange(len(keys)):
            t+='{}=?,'.format(keys[i])
        return t[:-1]

    def connect(self,dbname):
        self.con = sqlite3.connect(dbname,timeout=30,isolation_level='EXCLUSIVE')
        self.con.text_factory=str
        #self.con.row_factory=sqlite3.Row
        self.cur=self.con.cursor()
        
    def start_exclusive(self):
        self.con.execute('BEGIN EXCLUSIVE')
        
    def end_exclusive(self):
        self.commit()
        
    def commit(self):
        self.con.commit()
        
    def close(self):
        self.commit()
        self.con.close()
        
    def initdata(self,columndict):
        self.cur.execute('DROP TABLE IF EXISTS data')
        assert len(columndict.keys())
        schema="("
        for key,value in columndict.items():
            if len(schema) > 1:
                schema+=', '
            schema+= str(key+" "+value)
        schema+=")"
        self.cur.execute('CREATE TABLE data %s' %schema)
        self.commit()
    
    def add_column(self,columndict):
        assert len(columndict.keys())
        for key,value in columndict.items():
            self.cur.execute('ALTER TABLE data add column {} {}'.format(key,value))
        self.commit()
    
    def create_idx(self,row):
        self.cur.execute('CREATE INDEX {} on data({})'.format(row+'_idx',row))
        self.commit()
    
    def add_data(self,data,exclusive=False):
        if exclusive:
            self.start_exclusive()
        tmpl=self._templ1(len(data.keys()))
        for key in data.keys():
            if key[-2:]=='_z':
                data[key]=zlib.compress(data[key])
        self.cur.execute('INSERT INTO data({}) VALUES({})'.format(','.join(data.keys()),tmpl), data.values())
        if exclusive:
            self.end_exclusive()
            
    def update_data(self,data,where="",exclusive=False):
        if where:
            where="where %s" %where
        if exclusive:
            self.start_exclusive()
        tmpl=self._templ2(data.keys())
        for key in data.keys():
            if key[-2:]=='_z':
                data[key]=zlib.compress(data[key])
        v=list()
        for key,value in data.items():
            v.append(key)
            v.append(value)
        self.cur.execute('UPDATE data set {} {}'.format(tmpl, where), data.values())
        if exclusive:
            self.end_exclusive()
            
    def select_data(self,keys,where="",limit="",exclusive=False):
        if where:
            where="where {}".format(where)
        if limit:
            limit="limit %i" %limit
        if exclusive:
            self.start_exclusive()
        self.cur.execute('SELECT {} FROM data {} {}'.format(','.join(keys), where, limit))
        for values in self.cur.fetchall():
            res=dict()
            for i in xrange(len(keys)):
                if keys[i][-2:]=='_z':
                    res[keys[i]]=zlib.decompress(values[i])
                else:
                    res[keys[i]]=values[i]
            yield res
        if exclusive:
            self.end_exclusive()
            
    
if __name__ == '__main__':
    db=SQLdata(':memory:')
    db.initdata({'refcode':'TEXT',
                 'data':'TEXT',
                 'data_z':'BLOB'})
    db.add_data({'refcode':'ITEM1',
                 'data':'TEXT1',
                 'data_z':'TEXTZ1'}, exclusive=True)
    db.add_data({'refcode':'ITEM2',
                 'data':'TEXT2',
                 'data_z':'TEXTZ2'}, exclusive=True)
    db.update_data({'data':'TEXT1NEW',
                    'data_z':'TEXTZ1NEW'}, 'refcode="ITEM1"', exclusive=True)
    assert db.select_data(('refcode','data'), 'refcode="ITEM1"', limit=1).next() == \
                        {'refcode': 'ITEM1', 'data': 'TEXT1NEW'}
    assert db.select_data(('refcode','data','data_z'), 'refcode="ITEM1"', limit=1).next() == \
                        {'refcode': 'ITEM1', 'data': 'TEXT1NEW', 'data_z': 'TEXTZ1NEW'}
    assert db.select_data(('refcode','data','data_z'), 'refcode="ITEM2"', limit=1).next() == \
                        {'refcode': 'ITEM2', 'data': 'TEXT2', 'data_z': 'TEXTZ2'}