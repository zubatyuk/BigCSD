'''
Created on Aug 6, 2015

@author: roman
'''
from sql import SQLdata
from cStringIO import StringIO
import os, re
import numpy
from cctbx import sgtbx, xray

def get_section(fil,start,end,give_start=False,give_end=False):
    m1=re.compile(start)
    m2=re.compile(end)
    in_section=False
    for line in fil:
        if in_section:
            if m2.match(line):
                if give_end: yield line
                break
            yield line
        elif m1.match(line):
            in_section=True
            if give_start: yield line
    return

def mop2xyz(fil):
    cart=get_section(fil,'^                             CARTESIAN COORDINATES',
                         '^           Empirical Formula:')
    cart.next()
    res=list()
    for l in cart:
        ll=l.split()
        if len(ll)<4:
            break
        res.append((ll[1],tuple([float(i) for i in ll[2:5]])))
    return res
    

if __name__ == '__main__':
    import sys
    
    db=SQLdata(sys.argv[1])
    mopout=file(sys.argv[2]).read()
    refcode=os.path.splitext(os.path.basename(sys.argv[2]))[0]
    #print refcode

    data=db.select_data(('cif_z','smap_z'), 'refcode="{}"'.format(refcode)).next()
    
    smap=[(i.split()[0],sgtbx.rt_mx(i.split()[1])) for i in StringIO(data['smap_z'])]
    xyz=mop2xyz(StringIO(mopout))
    
    struct=xray.structure.from_cif(StringIO(data['cif_z'])).values()[0]
    frac=struct.unit_cell().fractionalize
    orth=struct.unit_cell().orthogonalize

    hcoords=dict()
    
    for i in xrange(len(xyz)):
        if xyz[i][0]!='H' and xyz[i][0]!='D':
            continue 
        if not hcoords.has_key(smap[i][0]):
            hcoords[smap[i][0]]=list()
        hcoords[smap[i][0]].append(smap[i][1].inverse()*frac(xyz[i  ][1]))
    
    mean_hc=dict()
    for k in hcoords:
        mean_hc[k]=numpy.mean(numpy.array(hcoords[k]),0)
    
    newstruct=struct.customized_copy()
    for a in newstruct.scatterers():
        if mean_hc.has_key(a.label):
            a.site=tuple(mean_hc[a.label])
    
    with open(refcode+'.cif', 'w') as f:
        newstruct.as_cif_simple(f)
        
    
        
    
#         #print k
#         a=numpy.array(hcoords[k])
#         #print a
#         m=numpy.mean(a,0)
#         #print m
#         
#         d=numpy.abs(a-m)
#         #print d
#         assert (d<0.2).all()
#         
#         c=numpy.array(orth(struct.select(struct.label_selection(k)).scatterers()[0].site))
#         print refcode, k, numpy.linalg.norm(m-c)
        
    print "OK"
         
#    for i in smap:
#        print i[0], i[1]
        
        
    
    
    
#     for i in StringIO(data['smap_z']):
#         i=i.split()
#         #i[1]=sgtbx.rt_mx(i[1])
#         if not i[0] in smap.keys():
#             smap[i[0]]=[i[1]]
#         else:
#             smap[i[0]].append(i[1])
#     
#     print smap
        
        
    
    
    
