'''
Created on Jul 30, 2015

@author: roman
'''

from run_tmpl import *
import sys
import os, tempfile, shutil

MOPAC='/worka/work/rzubatyu/mopac/MOPAC2012.exe'
MOPACENV={'MOPAC_LICENSE':'/worka/work/rzubatyu/mopac'}
RESDIR=os.path.join(os.getcwd(),'MOPOUT')
try:
    os.mkdir(RESDIR)
except:
    pass

db_data=sys.argv[1]
db_meta=sys.argv[2]
workdata=('refcode','mopin_z')

def dowork(data):
    cwd=os.getcwd()
    tmp=tempfile.mkdtemp(dir=cwd)
    os.chdir(tmp)
    inp=data['refcode']+'.mop'
    with open(inp,'w') as f:
        f.write(data['mopin_z'])
    os.environ.update(MOPACENV)
    try:
        os.system('{} {}'.format(MOPAC,inp))
    except:
        return False
    shutil.copy(data['refcode']+'.out',RESDIR)
    return True

run(db_data,db_meta,dowork,workdata)
        

