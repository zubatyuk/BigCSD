'''
Created on Jul 31, 2015

@author: roman
'''

from run_tmpl import *
import sys
import os
import tempfile, shutil

PLATON='d:\\scratch\\cryst\\platon\\platon.exe'
RESDIR=os.path.join(os.getcwd(),'PLATON')
try:
    os.mkdir(RESDIR)
except:
    pass

db_data=sys.argv[1]
db_meta=sys.argv[2]

workdata=('refcode','cif_z')

def dowork(data):
    print data['refcode']
    tmp=tempfile.mkdtemp(dir=os.getcwd())
    w=os.getcwd()
    os.chdir(tmp)
    
    with open(data['refcode']+'.cif','w') as f:
        f.write(data['cif_z'])
    
#    try:
#    os.system(PLATON+' -C '+ data['refcode']+'.cif')
#    os.system("echo exp1| "+PLATON+" "+data['refcode']+'_acc.cif')
    os.system(PLATON+' -c '+ data['refcode']+'.cif')

#         for fil in (data['refcode']+'_acc.cif',
#                     data['refcode']+'.cif',
#                     data['refcode']+'_acc_exp.ins',
#                     data['refcode']+'_acc_pl.spf'):
#             shutil.copy(fil, RESDIR)
#    with open(data['refcode']+'_acc.cif') as f:
#        acc=f.read()
#    with open(data['refcode']+'_acc_exp.ins') as f:
#        exp1=f.read()
#    with open(data['refcode']+'_acc_pl.spf') as f:
#        spf=f.read()
    with open(data['refcode']+'.lis') as f:
        lis=f.read()
    os.chdir(w)
    db=SQLdata(db_data)
#    db.update_data({'acc_z':acc, 'exp1_z':exp1, 'spf_z':spf, 'lis_z':lis}, 'refcode="{}"'.format(data['refcode']))
    db.update_data({'lis_z':lis}, 'refcode="{}"'.format(data['refcode']))
    db.close()
#    except:
#        return False

    #os.chdir("..")
    shutil.rmtree(tmp)
    return True

run(db_data,db_meta,dowork,workdata)