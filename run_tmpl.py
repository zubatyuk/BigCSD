'''
Created on Jul 30, 2015

@author: roman
'''
from sql import SQLdata
import os

def get_refcode(db_name):
    db=SQLdata(db_name)
    db.start_exclusive()
    d=[i for i in db.select_data(('refcode',), where='OK=0 and RUN=0', limit=1)]
    if len(d):
        d=d[0]
    else:
        return None
    db.update_data({'RUN':'1'}, 'refcode="{}"'.format(d['refcode']))
    db.end_exclusive()
    db.close()
    return d['refcode']

def get_data(db_name,refcode,keys):
    db=SQLdata(db_name)
    data=db.select_data(keys, where='refcode="{}"'.format(refcode), limit=1)
    return data

def update_OK(db_name,refcode):
    db=SQLdata(db_name)
    db.update_data({'OK':'1'}, 'refcode="{}"'.format(refcode))
    db.update_data({'RUN':'0'}, 'refcode="{}"'.format(refcode))
    db.close()

def run(db_data,db_meta,workfunc,workdata):
    cwd=os.getcwd()
    while True:
        os.chdir(cwd)
        refcode=get_refcode(db_meta)
        if not refcode:
            break
        data=get_data(db_data, refcode, workdata).next()
        if workfunc(data):
            os.chdir(cwd)
            update_OK(db_meta, refcode)

