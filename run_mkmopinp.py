'''
Created on Jul 31, 2015

@author: roman
'''

from run_tmpl import *
import sys
import os
from cctbx import sgtbx, xray
from cStringIO import StringIO 

db_data=sys.argv[1]
db_meta=sys.argv[2]
db_mopin=sys.argv[3]

workdata=('refcode','acc_z')

def dowork(data):
    print data['refcode']
    f=StringIO(data['acc_z'])
    try:
        struct=xray.structure.from_cif(f).values()[0]
    except:
        return False
    
    f.close()
    orth=struct.unit_cell().orthogonalize
    
    mop=StringIO()
    smap=StringIO()
    acif=StringIO()
    
    mop.write("NOOPT OPT-H GRAD=1.0 PM7 GEO-OK\n")
    mop.write(data['refcode']+'\n\n')
    atoms=struct.scatterers()
    for q in xrange(len(atoms)):
        a=atoms[q]
        symeq=struct.sym_equiv_sites(a.site)
        coords=symeq.coordinates()
        for w in xrange(len(coords)):
            mop.write('%-3s %12.8f 1 %12.8f 1 %12.8f 1\n' %((a.element_symbol(),)+orth(coords[w])))
            smap.write('%s %s\n' %(a.label, symeq.sym_op(w).as_xyz()))
    for v in ((1,0,0),(0,1,0),(0,0,1)):
        mop.write('Tv  %12.8f 1 %12.8f 1 %12.8f 1\n' %(orth(v)))
    
    struct.as_cif_simple(acif)
    
    db=SQLdata(db_mopin)
    db.add_data({'refcode':data['refcode'], 'mopin_z':mop.getvalue(), 'smap_z': smap.getvalue(), 'cif_z':acif.getvalue()})
    db.close()

    return True

run(db_data,db_meta,dowork,workdata)