'''
Created on Jul 29, 2015

@author: roman

'''
import sys
from sql import SQLdata

def iter_structs(allcif):
    buff=''
    struct=''
    with open(allcif) as fil:
        for line in fil:
            if line.startswith('data_CSD_CIF_'):
                if struct:
                    yield (struct,buff)
                buff=''
                struct=line.split('_')[-1].rstrip()
            buff+=line
        if buff:
            yield (struct,buff)

if __name__ == '__main__':
    allcif=sys.argv[1]
    dbname=sys.argv[2]

    schema={'refcode':'TEXT', 'cif_z':'BLOB'}
    db=SQLdata(dbname)
    db.initdata(schema)
    
    for refcode,cifstr in iter_structs(allcif):
        print refcode
        db.add_data({'refcode':refcode,'cif_z':cifstr})
    
    db.create_idx('refcode')
    db.close()
    print 'OK'
        
