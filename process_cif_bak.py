'''
Created on Jul 29, 2015

@author: roman

Read one big CSD CIF file, calculate properties using cctbx, output sqlite3 db 
'''

from iotbx import cif
import cctbx 
from sql import SQLdata
import sys, re
import numpy as np

CIFKEYS={'_journal_coden_Cambridge':('integer',int),
     '_journal_year':('integer',int),
     '_journal_name_full':('text',str),
     '_diffrn_ambient_temperature':('real',float),
     '_diffrn_ambient_pressure':('real',float),
     '_exptl_crystal_description':('text',str),
     '_cell_volume':('real',float),
     '_cell_formula_units_Z':('real',float),
     '_refine_ls_R_factor_gt':('real',float),
     '_symmetry_Int_Tables_number':('integer',int),
     '_diffrn_radiation_probe':('text',str),
     '_len_symmetry_equiv_pos_site_id':('integer',int),
     '_list_atom_type_symbol': ('text',str)
    }


def iter_structs(allcif):
    buff=''
    struct=''
    with open(allcif) as fil:
        for line in fil:
            if line.startswith('data_CSD_CIF_'):
                if struct:
                    yield (struct,buff)
                buff=''
                struct=line.split('_')[-1].rstrip()
            buff+=line
        if buff:
            yield (struct,buff)

def getkeys(cifdict):
    res={}
    for key in CIFKEYS.keys():
        if (key in cifdict.keys()) or \
                    (key.startswith('_len_') and key[4:] in cifdict.keys()) or \
                    (key.startswith('_list_') and key[5:] in cifdict.keys()):
            if key.startswith('_len_'):
                v=len(cifdict[key[4:]])
            elif key.startswith('_list_'):
                v=','.join(cifdict[key[5:]])+','
            else:
                v=cifdict[key]
            try:
                res[key]=CIFKEYS[key][1](v)
            except:
                pass
    return res

def mk_mop_inp(struct):
    struct_p1=struct.expand_to_p1()
    struct_p1ok=struct_p1.customized_copy(struct_p1)
    struct_p1ok.erase_scatterers()
    
    sites=np.array([[i for i in c] for c in struct_p1.sites_frac()])
    scatterers=struct_p1.scatterers()
    i=-1
    for site_i in sites:
        i+=1
        OK=True
        for site_j in sites[:i]:
            if (abs(site_i-site_j)<0.01).all() == True:
#            if np.isclose(sites[i], j, 0.001).all() == True:
                OK=False
                break
        if OK:
            struct_p1ok.add_scatterer(scatterers[i])

    mop=str()
    mop+='NOOPT OPT-H PM7 GEO-OK\n'
    mop+='HOPT\n'
    mop+='\n'
    
    orth=struct_p1ok.unit_cell().orthogonalize
    
    for a in struct_p1ok.scatterers():
        mop+='%-3s %12.8f 1 %12.8f 1 %12.8f 1\n' %((a.element_symbol(),)+orth(a.site))
    for v in ((1,0,0),(0,1,0),(0,0,1)):
        mop+='Tv  %12.8f 1 %12.8f 1 %12.8f 1\n' %(orth(v))
    return mop

def get_mol_map(struct):
    asu_mappings=struct.asu_mappings(buffer_thickness=3.5)
    bond_table = cctbx.crystal.pair_asu_table(asu_mappings) 
    bond_table.add_covalent_pairs(struct.scattering_types())
    pair_sym_table = bond_table.extract_pair_sym_table()
    clusters = cctbx.crystal.asu_clusters(bond_table,False)
    return clusters

if __name__ == '__main__':
    #allcif=sys.argv[1]
    allcif='R:\\OneDrive\\Documents\\BigCSD\\all1-ok.cif'
    i=0
    
    schema=dict()
    for key,value in CIFKEYS.items():
        schema[key]=value[0]
    
    schema['refcode']="TEXT"
    schema['cifz']="BLOB"
    schema['hasH']="TEXT"
    schema['numMols']="INTEGER"
    
    compress=('cifz',)
    
    db=SQLdata('R:\\OneDrive\\Documents\\BigCSD\\cifdata.db')
    db.initdata(schema)
    
    
    #schema_mop={'refcode': 'TEXT',
    #            'mopinpz': 'BLOB'}
    #db_mop=SQLdata('R:\\OneDrive\\Documents\\BigCSD\\mopin.db')
    #db_mop.initdata(schema_mop)
    
    for refcode,cifstr in iter_structs(allcif):
        i+=1
        print refcode
        if not i%1000:
            print i
            db.commit()
            #db_mop.commit()
            
        reader=cif.reader(input_string=cifstr)
        struct=reader.build_crystal_structures().values()[0]
        cifdict=reader.model().values()[0]
        
        #chech for disorder
        formula_struct=dict()
        formula_cifdict=dict()
        
        for q in struct.scattering_types_counts_and_occupancy_sums():
            formula_struct[q.scattering_type]=q.occupancy_sum
            
        zprime=int(cifdict['_cell_formula_units_Z'])/len(cifdict['_symmetry_equiv_pos_site_id'])
        r=re.compile('([a-zA-Z]+)(\d+\.?\d*)')
        for q in cifdict['_chemical_formula_sum'].split():
            m=r.match(q)
            formula_cifdict[m.group(1)]=float(m.group(2))*zprime
        
        if formula_struct.keys().sort() != formula_cifdict.keys().sort() or \
           0.95 < float(cifdict['_exptl_crystal_density_diffrn'])/struct.crystal_density() < 1.05: 
            print 'not ok'
            print ">>>", formula_struct.keys().sort(), formula_cifdict.keys().sort()
            print ">>>", formula_struct.items(), formula_cifdict.items()
            print ">>>", float(cifdict['_exptl_crystal_density_diffrn']),struct.crystal_density()
            continue
        else:
            print refcode, 'ok'
        
        data=getkeys(cifdict)
        
        for key in CIFKEYS.keys():
            if not key in data.keys():
                data[key]=None
                
        data['refcode']=refcode
        data['cifz']=cifstr
        data['hasH']=('H,' in data['_list_atom_type_symbol']) or \
                     ('D,' in data['_list_atom_type_symbol'])
                     
        data['numMols']=len(get_mol_map(struct).index_groups)

        db.add_data(data, compress)
        
        #if data['hasH']:
        #    mop=mk_mop_inp(struct)
        #    db_mop.add_data({'refcode':refcode,
        #                     'mopinpz':mop}, compress=('mopinpz',))
                
    db.close()
    #db_mop.close()
        
    print 'OK'
        
