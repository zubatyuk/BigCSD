'''
Created on Aug 7, 2015

@author: roman
'''

import re, sys

def get_section(fil,start,end,give_start=False,give_end=False):
    m1=re.compile(start)
    m2=re.compile(end)
    in_section=False
    for line in fil:
        if in_section:
            if m2.match(line):
                if give_end: yield line
                break
            yield line
        elif m1.match(line):
            in_section=True
            if give_start: yield line
    return

if __name__ == '__main__':
    #re_atom=re.compile('-     ([a-zA-Z]+)(\?\w+\))?\w+\s+(-?[\d+\.\d+])\s(-?[\d+\.\d+])\s(-?[\d+\.\d+])\s(-?[\d+\.\d+])\s(-?[\d+\.\d+])\s(-?[\d+\.\d+])\s     1  1     1          1\s(\S+)')
    re_resi=re.compile('^\*{56} Residue =\s+(\d+)')
    re_atom=re.compile('-     ([a-zA-Z]+)(?:\((\w+)\))?\w*\s+(?:-?\d+\.\d+\s+){6}(?:1\s+){4}(\S+)')
    re_symm=re.compile('^[a-z]? =\[\s*\d+\.\d+\] = \[\s*(\d+_\d+)\] =(\S+)')
    with open(sys.argv[1]) as f:
        sect=get_section(f,
                         '^Flags Label         Fractional Coordinates',
                         '^   Ordered Structure')
        resi=0
        data=[[]]
        symmdict=dict()
        for l in sect:
            m=re_resi.match(l)
            if m:
                resi+=1
                data.append([])
                continue
            m=re_atom.match(l)
            if m:
                label=m.group(1)
                if m.group(2):
                    label+=m.group(2)
                symm=m.group(3).replace('.','_')
                if symm=='-':
                    symm=None
                data[resi].append([label,symm])
                continue
            m=re_symm.match(l)
            if m:
                symmdict[m.group(1)]=m.group(2)
    for resi in data:
        print ">>>"
        for atom in resi:
            if atom[1]:
                atom[1]=symmdict[atom[1]]
            print atom
            
                
